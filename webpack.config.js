var path = require('path');

module.exports = {
  entry: './src/index.ts',

  resolve: {
    extensions: ['', '.ts', '.webpack.js', '.web.js', '.js'],
    alias: {
      "src": path.join(__dirname, "src"),
      "math": path.join(__dirname, "src", "math"),
      "draw": path.join(__dirname, "src", "draw"),
      "io": path.join(__dirname, "src", "io")
    }
  },

  output:{
    path: './_build',
    filename: 'all.js',
    publicPath: '/assets'
  },

  devtool: 'inline-source-map',

  module: {
    loaders: [
      {
        test: /\.ts$/,
        loader: 'awesome-typescript-loader'
      }
    ]
  }
};