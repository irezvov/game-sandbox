export enum KeyCode {
  ArrowUp = 38,
  ArrowDown = 40,
  ArrowLeft = 37,
  ArrowRight = 39,
  Space = 32
}

var pressed: KeyCode[] = [];

export function isPressed(k: KeyCode) : boolean {
  return pressed.indexOf(k) >= 0;
}

function bindKeys() {
  document.addEventListener('keydown', (e) => {
    pressed.push(e.keyCode);
  }, true);
  document.addEventListener('keyup', (e) => {
    pressed = pressed.filter(k => k != e.keyCode);
  }, true);
}

window.addEventListener('load', () => {
  bindKeys();
}, false);
