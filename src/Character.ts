import {Vec2d, vec2d} from 'math/Vec2d';
import {AABB} from 'math/AABB';
import {GameObject, IGameObject, ISolidGameObject} from 'src/GameObject';
import {GameMap} from 'src/GameMap';
import {KeyCode, isPressed} from 'io/Keyboard';
import {AnimatedSprite} from 'draw/Sprite';

const E = 0.001;
const G = new Vec2d(0, -2);

const JUMP_MULTIPLIER = 12;
const WALK_MULTIPLIER = 5;

export class Character extends GameObject implements ISolidGameObject {
  public width: number;
  public height: number;
  public jumpVelocity: Vec2d;
  private collisions: ISolidGameObject[];
  private map: GameMap;

  private walkSprite = new AnimatedSprite('/img/character_actions.png', [
    [4,0], [5,0], [6,0], [7,0], [0,1], [1,1], [2,1], [3,1]
  ]);
  private staySprite = new AnimatedSprite('/img/character_actions.png', [[0,8]]);

  constructor(map: GameMap, width: number, initialPosition?: Vec2d) {
    super(initialPosition);
    this.width = width;
    this.height = width*2;
    this.jumpVelocity = Vec2d.zero;
    this.map = map;
  }

  draw(ctx: CanvasRenderingContext2D) {
    const w = this.width;
    const leftCorner = this.position.plus(Vec2d.single(w).fact(-0.5));
    const sprite = this.walkSprite.started ? this.walkSprite.canvas : this.staySprite.canvas;
    ctx.drawImage(sprite, leftCorner.x, leftCorner.y, w, w);
  }

  protected getWalkVelocity() {
    let basic: Vec2d = Vec2d.zero;
    if (isPressed(KeyCode.ArrowLeft) && this.canWalkToLeft()) {
      if (this.walkSprite.direction) {
        this.walkSprite.mirror()
        this.staySprite.mirror()
      }
      basic = basic.plus(new Vec2d(-this.width*WALK_MULTIPLIER, 0));
    }

    if (isPressed(KeyCode.ArrowRight) && this.canWalkToRight()) {
      if (!this.walkSprite.direction) {
        this.walkSprite.mirror()
        this.staySprite.mirror()
      }
      basic = basic.plus(new Vec2d(this.width*WALK_MULTIPLIER, 0));
    }

    if (basic.x) {
      this.walkSprite.start();
    }
    else {
      this.walkSprite.stop();
    }

    return basic.fact(this.isOnGround() ? 1 : 0.5);
  }

  protected getJumpVelocity(dt: number) {
    if (isPressed(KeyCode.Space) && this.isOnGround() && this.canJump()) {
      this.jumpVelocity = new Vec2d(0, this.width*10).plus(this.getWalkVelocity().fact(0.5));
    }
    else if (this.isOnGround()) {
      this.jumpVelocity = Vec2d.zero;
    }
    else {
      this.jumpVelocity = this.jumpVelocity.plus(G.fact(dt/1000));
    }

    if (!this.canWalkToLeft() && this.jumpVelocity.x < 0) {
      this.jumpVelocity.x = 0;
    }

    if (!this.canWalkToRight() && this.jumpVelocity.x > 0) {
      this.jumpVelocity.x = 0;
    }

    if (!this.canJump() && this.jumpVelocity.y > 0) {
      this.jumpVelocity.y = 0;
    }

    return this.jumpVelocity;
  }

  protected getGround() {
    const grounds = this.collisions.filter(obj => {
      const thisBox = this.getBox();
      const otherBox = obj.getBox();
      return Math.abs(otherBox.topLeft.y - thisBox.bottomRight.y) < E
        && (inInterval(thisBox.topLeft.x, otherBox.topLeft.x, otherBox.bottomRight.x)
          || inInterval(thisBox.bottomRight.x, otherBox.topLeft.x, otherBox.bottomRight.x)
        )
    });
    if (grounds.length) {
      return grounds[0];
    }
    if (Math.abs(this.position.y - this.width/2 + 1) < E) {
      return null;
    }
    return null;
  }

  update(dt: number) {
    this.collisions = this.map.getCollisions(this);
    const d = this.getJumpVelocity(dt).plus(this.getWalkVelocity()).fact(dt/1000);
    this.position = this.position.plus(d);
    if (this.isOnGround() && this.jumpVelocity.y <= 0) {
      this.position.y = this.getGroundLevel() + this.width/2;
    }
    this.collisions = null;
  }

  isOnGround() {
    return Math.abs(this.position.y - this.width/2 + 1) < E || this.getGround();
  }

  getGroundLevel() {
    let ground = this.getGround();
    if (!ground) {
      return -1;
    }
    return ground.getBox().topLeft.y;
  }

  canWalkToLeft() {
    return 1 + this.position.x - this.width/2 > E
      && !this.collisions.filter(obj => {
        let thisBox = this.getBox();
        let otherBox = obj.getBox();
        return Math.abs(otherBox.bottomRight.x - thisBox.topLeft.x) < E
          && thisBox.bottomRight.y < otherBox.topLeft.y
          && thisBox.topLeft.y > otherBox.bottomRight.y
      }).length;
  }

  canWalkToRight() {
    return 1 - this.position.x - this.width/2 > E
      && !this.collisions.filter(obj => {
        let thisBox = this.getBox();
        let otherBox = obj.getBox();
        return Math.abs(otherBox.topLeft.x - thisBox.bottomRight.x) < E
          && thisBox.bottomRight.y < otherBox.topLeft.y
          && thisBox.topLeft.y > otherBox.bottomRight.y
      }).length;
  }

  canJump(): boolean {
    return 1 + this.position.y + this.width/2 < 2 - E
      && !this.collisions.filter(obj => {
        let thisBox = this.getBox();
        let otherBox = obj.getBox();
        return Math.abs(otherBox.bottomRight.y - thisBox.topLeft.y) < E
          && (inInterval(thisBox.topLeft.x, otherBox.topLeft.x, otherBox.bottomRight.x)
            || inInterval(thisBox.bottomRight.x, otherBox.topLeft.x, otherBox.bottomRight.x)
          )
      }).length;
  }

  isSolid() {
    return true;
  }

  getBox(): AABB {
    return new AABB(this.position, this.width, this.width);
  }

  getBorderPointByDirection(dirPoint: Vec2d): Vec2d {
    let dir = dirPoint.plus(this.position.fact(-1)).normalize();
    let borderNormal = new Vec2d(
      Math.abs(dir.x) >= Math.abs(dir.y) ? sign(dir.x) * 1 : 0,
      Math.abs(dir.y) >= Math.abs(dir.x) ? sign(dir.y) * 1 : 0
    );
    let cosAlpha = borderNormal.dot(dir);
    let dirLength = this.width/(2*cosAlpha);
    return this.position.plus(dir.fact(dirLength));
  }
}

function sign(n: number) {
  return n > 0 ? 1 : (n == 0 ? 0 : -1);
}

function inInterval(x: number, left: number, right: number) : boolean {
  return x > left && x < right;
}