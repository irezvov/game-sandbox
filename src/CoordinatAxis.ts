import {Vec2d, vec2d} from 'math/Vec2d';
import {AABB} from 'math/AABB';
import {GameObject} from 'src/GameObject';
import {Scene} from 'src/Scene';

export class CoordinatAxis extends GameObject {

  public scene: Scene;

  constructor(scene: Scene) {
    super(Vec2d.zero);
    this.scene = scene;
    this.zIndex = -1;
  }

  getBox(): AABB {
    return new AABB(Vec2d.zero, 2, 2);
  }

  line(ctx: CanvasRenderingContext2D, from: Vec2d, to: Vec2d, width = 1, color = '#000') {
    ctx.lineWidth = width/this.scene.width;
    ctx.moveTo(from.x, from.y);
    ctx.lineTo(to.x, to.y);
    ctx.strokeStyle = color;
    ctx.stroke();
  }

  draw(ctx: CanvasRenderingContext2D) {
    this.line(ctx, new Vec2d(-1, 0), new Vec2d(1, 0));
    this.line(ctx, new Vec2d(0, -1), new Vec2d(0, 1));
  }
}