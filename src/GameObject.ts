import {Vec2d, vec2d} from 'math/Vec2d';
import {AABB} from 'math/AABB';

export interface IGameObject {
  position: Vec2d;
  zIndex: number;
  draw(ctx: CanvasRenderingContext2D): void;
  update(dt: number): void;
  isSolid(): boolean;
  getBox(): AABB;
}

export interface ISolidGameObject extends IGameObject {
  getBorderPointByDirection(dir: Vec2d): Vec2d;
}

export class GameObject implements IGameObject {
  public position: Vec2d;
  public zIndex: number = 0;

  constructor(initialPosition?: Vec2d) {
    this.position = initialPosition || Vec2d.zero;
  }

  draw(ctx: CanvasRenderingContext2D) {
    throw new Error('Draw callback not implemented');
  }

  update(dt: number) {
    // do nothing
  }

  isSolid() {
    return false;
  }

  getBox(): AABB {
    throw new Error('Get box not implemented for non-solid shapes');
  }
}
