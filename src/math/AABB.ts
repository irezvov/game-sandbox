import {Vec2d, vec2d} from 'math/Vec2d';

function inInterval(x: number, left: number, right: number) : boolean {
  return x > left && x < right;
}

function sign(n: number) {
  return n > 0 ? 1 : (n == 0 ? 0 : -1);
}

export class AABB {
  public topLeft: Vec2d;
  public bottomRight: Vec2d;
  public center: Vec2d;

  constructor(center: Vec2d, public width: number, public height: number) {
    this.center = center;
    this.topLeft = vec2d(center.x - width/2, center.y + height/2);
    this.bottomRight = vec2d(center.x + width/2, center.y - height/2);
  }

  isIntersect(another: AABB) {
    return (inInterval(this.topLeft.x, another.topLeft.x, another.bottomRight.x)
      || inInterval(this.bottomRight.x, another.topLeft.x, another.bottomRight.x)
      || inInterval(another.topLeft.x, this.topLeft.x, this.bottomRight.x)
      || inInterval(another.bottomRight.x, this.topLeft.x, this.bottomRight.x)
    ) && (
      inInterval(this.topLeft.y, another.bottomRight.y, another.topLeft.y)
      || inInterval(this.bottomRight.y, another.bottomRight.y, another.topLeft.y)
      || inInterval(another.topLeft.y, this.bottomRight.y, this.topLeft.y)
      || inInterval(another.bottomRight.y, this.bottomRight.y, this.topLeft.y)
    )
  }

  move(d: Vec2d): AABB {
    return new AABB(this.center.plus(d), this.width, this.height);
  }

  bounded(top: number, right: number, bottom: number, left: number): AABB {
    let dx = 0;
    let dy = 0;
    if (this.topLeft.x < left) {
      dx = Math.abs(this.topLeft.x - left);
    }
    if (this.bottomRight.x > right) {
      dx = -Math.abs(this.bottomRight.x - right);
    }
    if (this.topLeft.y > top) {
      dy = -Math.abs(this.topLeft.y - top);
    }
    if (this.bottomRight.y < bottom) {
      dy = Math.abs(this.bottomRight.y - bottom);
    }

    return new AABB(vec2d(this.topLeft.x + dx + this.width/2, this.topLeft.y + dy - this.height/2), this.width, this.height);
  }

  getBorderPointByDirection(dirPoint: Vec2d): Vec2d {
    let dir = dirPoint.plus(this.center.fact(-1)).normalize();
    let borderNormal = new Vec2d(
      Math.abs(dir.x) >= Math.abs(dir.y) ? sign(dir.x) * 1 : 0,
      Math.abs(dir.y) >= Math.abs(dir.x) ? sign(dir.y) * 1 : 0
    );
    let cosAlpha = borderNormal.dot(dir);
    let dirLength = this.width/(2*cosAlpha);
    return this.center.plus(dir.fact(dirLength));
  }
}