import {Scene} from 'src/Scene';
import {ISolidGameObject, IGameObject, GameObject} from 'src/GameObject';
import {Camera} from 'draw/Camera';

interface IGameObjectDescription {
  position: { x: number; y: number };
  type: string;
  params: any;
}

interface IGameMapDescription {
  width: number;
  height: number;
  objects: IGameObjectDescription[];
}

var map: IGameMapDescription = {
  width: 20,
  height: 10,
  objects: [
    {
      type: 'square',
      position: { x: -5, y: -9 },
      params: {
        width: 1,
        interactive: true
      }
    },
    {
      type: 'square',
      position: { x: 5, y: -9 },
      params: {
        width: 1,
        interactive: false,
        color: '#5C6BC0'
      }
    }
  ]
};

export class GameMap {
  public width: number;
  public height: number;
  public objects: IGameObject[] = [];

  constructor(width: number, height?: number) {
    this.width = width;
    this.height = typeof height == 'number' ? height : width;
  }

  load(mapDescr: IGameMapDescription) {
    mapDescr.objects.forEach((obj) => {
      // let instance = GameObject.fromDescription(obj, this);
      // if (instance) {
      //   this.objects.push(instance);
      // }
    });
  }

  update(dt: number) {
    let os = this.objects;
    for(let i = 0, l = this.objects.length; i < l; ++i) {
      os[i].update(dt);
    }
  }

  draw(ctx: CanvasRenderingContext2D, camera: Camera) {
    let os = this.objects;
    for(let i = 0, l = this.objects.length; i < l; ++i) {
      if(camera.isVisible(os[i])) {
        os[i].draw(ctx);
      }
    }
  }

  unitsToNormal(u: number): number {
    return u * 2/Math.max(this.width, this.height);
  }

  getCollisions(o: ISolidGameObject): ISolidGameObject[] {
    return <ISolidGameObject[]>this.objects.filter(obj => obj !== o && obj.isSolid())
      .filter((obj:ISolidGameObject) =>
        o.getBorderPointByDirection(obj.position)
        .plus(obj.getBorderPointByDirection(o.position).fact(-1)).length() < o.getBox().width/100
      );
  }

  add(obj: IGameObject) {
    this.objects.push(obj);
    this.objects.sort((a, b) => a.zIndex - b.zIndex);
  }
}