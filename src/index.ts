import {Vec2d, vec2d} from 'math/Vec2d';
import {AABB} from 'math/AABB';
import {TrackableCamera} from 'draw/Camera';
import {GameObject, IGameObject, ISolidGameObject} from 'src/GameObject';
import {GameMap} from 'src/GameMap';
import {Scene} from 'src/Scene';
import {CoordinatAxis} from 'src/CoordinatAxis';
import {Square} from 'src/Square';
import {Character} from 'src/Character';

const PHISICAL_WIDTH = 1100;
const PHISICAL_HEIGHT = 600;

window.addEventListener('load', () => {
  let scene = new Scene(PHISICAL_WIDTH, PHISICAL_HEIGHT, 'screen');
  let character = new Character(scene.map, 0.07, new Vec2d(0, -0.8));
  scene.map.add(character);
  scene.map.add(new CoordinatAxis(scene));
  scene.map.add(new Square(scene.map, 0.1, new Vec2d(0.5, -0.8), '#5C6BC0'))
  scene.setCamera(new TrackableCamera(0.5, character, scene));
  scene.onClick((pos) => scene.map.add(new Square(scene.map, 0.1, pos, '#2196F3')));
  scene.run();
}, false);