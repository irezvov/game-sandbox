export class Sprite {
  protected image = new Image();
  public canvas = document.createElement('canvas');
  public direction = true;
  protected ctx: CanvasRenderingContext2D;
  public loaded: boolean = false;

  constructor(url: string) {
    this.image.src = url;
    this.image.onload = this.onImageLoad.bind(this);
  }

  onImageLoad() {
    this.loaded = true;
    this.canvas.width = this.image.width;
    this.canvas.height = this.image.height;
    this.ctx = this.canvas.getContext('2d');
    this.ctx.save();
    this.ctx.scale(1, -1);
    this.ctx.drawImage(this.image, 0, -this.image.height, this.image.width, this.image.height);
    this.ctx.restore();
  }

  mirror() {
    this.direction = !this.direction;
    this.ctx.clearRect(0, 0, this.image.width, this.image.height)
    this.ctx.save();
    this.ctx.scale(!this.direction ? -1 : 1, -1);
    this.ctx.drawImage(this.image, !this.direction ? -this.image.width : 0, -this.image.height, this.image.width, this.image.height);
    this.ctx.restore()
  }
}

export class AnimatedSprite extends Sprite {
  private points: number[][];
  private currentPoint = 0;
  private frame: number;
  public started: boolean = false;
  private timer: number;

  constructor(url: string, points: number[][], frame = 64) {
    super(url);
    this.points = points;
    this.frame = frame;
  }

  start() {
    if (this.started) {
      return;
    }
    this.started = true;
    this.timer = setInterval(() => {
      if (++this.currentPoint >= this.points.length) {
        this.currentPoint = 0;
      }
      this.draw()
    }, 100);
  }

  stop() {
    this.started = false;
    clearInterval(this.timer);
    this.currentPoint = 0;
    this.draw()
  }

  draw() {
    if (!this.loaded) {
      return;
    }
    this.ctx.clearRect(0, 0, 64, 64);
    this.ctx.save();
    this.ctx.scale(!this.direction ? -1 : 1, -1);
    this.ctx.drawImage(this.image,
      this.points[this.currentPoint][0]*this.frame,
      this.points[this.currentPoint][1]*this.frame,
      this.frame,
      this.frame,
      !this.direction ? -this.frame : 0,
      -this.frame,
      this.frame,
      this.frame
    );
    this.ctx.restore();
  }

  onImageLoad() {
    this.loaded = true;
    this.canvas.width = this.frame;
    this.canvas.height = this.frame;
    this.ctx = this.canvas.getContext('2d');
    this.draw();
  }

  mirror() {
    this.direction = !this.direction;
    this.draw();
  }
}