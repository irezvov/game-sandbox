import {Vec2d, vec2d} from 'math/Vec2d';
import {AABB} from 'math/AABB';

import {IGameObject} from 'src/GameObject';
import {Scene} from 'src/Scene';

export class Camera {
  public box: AABB;
  protected scene: Scene;

  constructor(position: Vec2d, height: number, scene: Scene) {
    let width = scene.width/scene.heihgt * height;
    this.box = new AABB(position, width, height);
    this.scene = scene;
  }

  public isVisible(obj: IGameObject): boolean {
    return this.box.isIntersect(obj.getBox());
  }

  update(dt: number) { }

  apply(ctx: CanvasRenderingContext2D) {
    ctx.setTransform(
      this.scene.backWidth/this.box.width, 0,                    // scaleX
      0, -this.scene.backHeight/this.box.height,                 // scaleY
      this.scene.backWidth/this.box.width*(-this.box.topLeft.x), // dx
      this.scene.backHeight/this.box.height*this.box.topLeft.y   // dy
    );
  }

  screenToWorld(point: Vec2d): Vec2d {
    let ww = Math.abs(this.box.topLeft.x - this.box.bottomRight.x);
    let wh = Math.abs(this.box.topLeft.y - this.box.bottomRight.y);
    return this.box.topLeft.plus(new Vec2d(point.x/this.scene.width*ww, point.y/this.scene.heihgt*(-wh)));
  }
}

export class TrackableCamera extends Camera {
  public trackable: IGameObject;

  constructor(height: number, trackable: IGameObject, scene: Scene) {
    super(trackable.position, height, scene);
    this.trackable = trackable;
  }

  update(dt: number) {
    this.box = new AABB(this.trackable.position, this.box.width, this.box.height).bounded(1, 1, -1, -1);
  }
}