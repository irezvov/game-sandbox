import {GameMap} from 'src/GameMap';
import {Camera} from 'draw/Camera';
import {Vec2d, vec2d} from 'math/Vec2d';

const UPDATE_GRANULARITY = 1;

export class Scene {
  public width: number;
  public heihgt: number;

  public backWidth: number;
  public backHeight: number;
  public canvas: HTMLCanvasElement;
  public ctx: CanvasRenderingContext2D;

  public map: GameMap;
  public camera: Camera = null;

  private lastDrawingTime: number;
  private drawTimer: number;

  private updateFn: FrameRequestCallback;

  constructor(width: number, heihgt: number, cls?: string) {
    this.width = width;
    this.heihgt = heihgt;

    this.backWidth = width * window.devicePixelRatio;
    this.backHeight = heihgt * window.devicePixelRatio;

    this.createCanvas(cls);
    this.updateFn = this.update.bind(this);
    this.map = new GameMap(1);
  }

  private createCanvas(cls?: string) {
    this.canvas = document.createElement("canvas");
    document.body.appendChild(this.canvas);
    if (cls) {
      this.canvas.classList.add(cls);
    }
    this.canvas.width = this.backWidth;
    this.canvas.height = this.backHeight;
    this.canvas.style.width = this.width + 'px';
    this.canvas.style.height = this.heihgt + 'px';
    this.ctx = this.canvas.getContext("2d");
  }

  private visibilityCheck() {
    console.log('hidden: ', document.hidden)
    if (document.hidden) {
      window.cancelAnimationFrame(this.drawTimer);
      this.drawTimer = setTimeout(this.updateFn, 1000)
    }
    else {
      clearTimeout(this.drawTimer);
      this.drawTimer = window.requestAnimationFrame(this.updateFn);
    }
  }

  public update() {
    let now = Date.now();
    let dt = now - this.lastDrawingTime;
    this.lastDrawingTime = now;
    for (let j = 0, count = Math.ceil(dt/UPDATE_GRANULARITY); j < count; ++j) {
      let ddt = Math.min(UPDATE_GRANULARITY, dt - j*UPDATE_GRANULARITY);
      this.map.update(ddt);
    }

    if (!document.hidden) {
      this.ctx.clearRect(-1, -1, 2, 2);
      this.ctx.beginPath();
      this.camera.update(dt);
      this.camera.apply(this.ctx);

      this.map.draw(this.ctx, this.camera);
      this.ctx.closePath();
      this.drawTimer = window.requestAnimationFrame(this.updateFn);
    }
    else {
      this.drawTimer = setTimeout(this.updateFn, 1000);
    }
  }

  public run() {
    this.lastDrawingTime = Date.now();
    document.addEventListener('visibilitychange', this.visibilityCheck.bind(this), false);
    this.visibilityCheck();
  }

  public setCamera(camera: Camera) {
    this.camera = camera;
  }

  public onClick(handler: (pos: Vec2d) => any) {
    this.canvas.addEventListener('click', (e) => {
      let rect = (<HTMLElement>e.target).getBoundingClientRect();
      let x = e.clientX - rect.left;
      let y = e.clientY - rect.top;
      handler(this.camera.screenToWorld(vec2d(x, y)));
    }, false);
  }
}