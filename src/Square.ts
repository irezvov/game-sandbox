import {Vec2d, vec2d} from 'math/Vec2d';
import {AABB} from 'math/AABB';
import {GameObject, IGameObject, ISolidGameObject} from 'src/GameObject';
import {GameMap} from 'src/GameMap';

const E = 0.001;
const G = new Vec2d(0, -1).fact(2);

function sign(n: number) {
  return n > 0 ? 1 : (n == 0 ? 0 : -1);
}

function inInterval(x: number, left: number, right: number) : boolean {
  return x > left && x < right;
}

export class Square extends GameObject {
  public width: number;
  private collisions: ISolidGameObject[];
  private fallVelocity = Vec2d.zero;
  public color: string;
  private map: GameMap;

  constructor(map: GameMap, width: number, initialPosition?: Vec2d, color: string = '#ff1133') {
    super(initialPosition);
    this.width = width;
    this.color = color;
    this.map = map;
  }

  draw(ctx: CanvasRenderingContext2D) {
    let leftCorner = this.position.plus(Vec2d.single(this.width).fact(-0.5));
    ctx.fillStyle = this.color;
    ctx.fillRect(leftCorner.x, leftCorner.y, this.width, this.width);
  }

  protected getJumpVelocity(dt: number) {
    this.fallVelocity = this.isOnGround() ? Vec2d.zero : this.fallVelocity.plus(G.fact(dt/1000));
    return this.fallVelocity;
  }

  protected getGround() {
    let grounds = this.collisions.filter(obj => {
      let thisBox = this.getBox();
      let otherBox = obj.getBox();
      return Math.abs(otherBox.topLeft.y - thisBox.bottomRight.y) < E
        && (inInterval(thisBox.topLeft.x, otherBox.topLeft.x, otherBox.bottomRight.x)
          || inInterval(thisBox.bottomRight.x, otherBox.topLeft.x, otherBox.bottomRight.x)
        )
    });
    if (grounds.length) {
      return grounds[0];
    }
    if (Math.abs(this.position.y - this.width/2 + 1) < E) {
      return null;
    }
    return null;
  }

  update(dt: number) {
    this.collisions = this.map.getCollisions(this);
    let d = this.getJumpVelocity(dt).fact(dt/1000);
    this.position = this.position.plus(d);
    if (this.isOnGround()) {
      this.position.y = this.getGroundLevel() + this.width/2;
    }
    this.collisions = null;
  }

  isOnGround() {
    return Math.abs(this.position.y - this.width/2 + 1) < E || this.getGround();
  }

  getGroundLevel() {
    let ground = this.getGround();
    if (!ground) {
      return -1;
    }
    return ground.getBox().topLeft.y;
  }

  isSolid() {
    return true;
  }

  getBox(): AABB {
    return new AABB(this.position, this.width, this.width);
  }

  getBorderPointByDirection(dirPoint: Vec2d): Vec2d {
    let dir = dirPoint.plus(this.position.fact(-1)).normalize();
    let borderNormal = new Vec2d(
      Math.abs(dir.x) >= Math.abs(dir.y) ? sign(dir.x) * 1 : 0,
      Math.abs(dir.y) >= Math.abs(dir.x) ? sign(dir.y) * 1 : 0
    );
    let cosAlpha = borderNormal.dot(dir);
    let dirLength = this.width/(2*cosAlpha);
    return this.position.plus(dir.fact(dirLength));
  }
}